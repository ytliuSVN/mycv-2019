const LOCALIZE = {
    de: {
        title: 'Ausbildung',
        year_2014: {
            c1_certificate: "B1-Zertifizierung der Kultusministerkonferenz in deutscher Sprache",
            brevet: "Mittlere Reife mit Prädikat 'Sehr gut'",
        },
        year_2017: {
            bac: {
                title: "Wissenschaftliches Abitur",
                complement: "(Option Europäische Sektion Physik und Chemie in Deutsch sowie Sprachen und Kulturen der Antike: Latein) mit Prädikat '<i>Gut</i>'",
            },
            start_isfates: {
                title: "Beginn des Informatikstudiums beim <a href=\"https://www.dfhi-isfates.eu/de/\" target=\"_blank\">DFHI</a>",
                complement: "Deutsch-Französisches Hochschulinstitut für Technik und Wirtschaft",
            },
        },
        year_2018: {
            htw: {
                title: "Zweites Studienjahr an der HTW in Saarbrücken, Deutschland",
            },
        },
    },
    fr: {
        title: 'Mon parcours',
        year_2014: {
            c1_certificate: "Certification B1 en allemand, KMK",
            brevet: "Obtention du brevet des collèges, mention 'Très bien'",
        },
        year_2017: {
            bac: {
                title: "Obtention du BAC S (scientifique)",
                complement: "(option section européenne physique-chimie en allemand et Langues et cultures de l'antiquité: latin) avec mention '<i>Bien</i>'",
            },
            start_isfates: {
                title: "Début de mes études en informatique à l'<a href=\"https://www.dfhi-isfates.eu/fr/\" target=\"_blank\">ISFATES</a>",
                complement: "Institut Supérieur Franco-Allemand de Techniques, d'Economie et de Sciences",
            },
        },
        year_2018: {
            htw: {
                title: "Deuxième année d'études à la HTW à Sarrebruck en Allemagne",
            },
        },
    },
    en: {
        title: 'Education',
        year_2014: {
            c1_certificate: "B1 certification in German, KMK",
            brevet: "Obtaining the certificate of the Middle School, mention 'Very good'",
        },
        year_2017: {
            bac: {
                title: "Obtaining the BAC S (scientific)",
                complement: "(option European section physics and chemistry in German and Languages and cultures of antiquity: Latin) with the mention '<i>Bien</i>'",
            },
            start_isfates: {
                title: "Start of my studies in computer science at <a href=\"https://www.dfhi-isfates.eu/fr/\" target=\"_blank\">ISFATES</a>",
                complement: "Institut Supérieur Franco-Allemand de Techniques, d'Economie et de Sciences",
            },
        },
        year_2018: {
            htw: {
                title: "Second year of study at HTW in Saarbrücken, Germany",
            },
        },
    }
}

export default LOCALIZE;