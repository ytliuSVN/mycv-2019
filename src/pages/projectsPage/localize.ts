const LOCALIZE = {
    de: {
        title: "Meine Projekte",
        project_caretaker: {
            summary: "Dies ist mein Projekt des sechsten Semesters bei der DFHI. Ziel war es, ein Smart Home Modell zu entwickeln, das mit einer fortschrittlichen Webanwendung verbunden ist. Ich war Projektleiter und IT-Manager des Projekts.",
        },
        project_adventskalender: {
            summary: "IT-Manager für die Erstellung einer progressiven Web-App, die einen Adventskalender für DFHI-Studenten simuliert. Das gesammelte Geld wird an mehrere Vereine gespendet.",
        },
        project_astropulse: {
            summary: "Erstellung einer Online-Umfrage, welche zeigt, dass die Frequenz eines Pulsars unsere Wahrnehmung von Zeit beeinflussen kann. Projekt, das im Rahmen meines Studiums durchgeführt wurde.",
        },
        project_diva: {
            summary: "Refactoring von Frontend und Backend der offiziellen Website der DFHI-Studentenvereinigung: die DIVA.",
        },
        project_walkinlove: {
            summary: "Realisierung einer Website mit php im Rahmen meines Studiums.",
        },
        project_zukunftsforum: {
            summary: "Aufbau einer Wordpress-Website und einer Grafikcharta für den Verein 'Zukunftsforum'.",
        }
    },
    fr: {
        title: "Mes projets",
        project_caretaker: {
            summary: "Projet de 3ème année à l'ISFATES. Objectif: créer un modèle de maison intelligente connectée à une application web progressive. J'ai rempli les rôles de chef de projet et responsable informatique du projet.",
        },
        project_adventskalender: {
            summary: "Responsable informatique de la création d'une progressive web app simulant un calendrier de l'avent pour les étudiants de l'ISFATES. L'argent récolté sera reversé à plusieurs associations.",
        },
        project_astropulse: {
            summary: "Création d'une enquête en ligne démontrant que la fréquence d'un pulsar peut influencer notre perception du temps. Projet réalisé dans le cadre de mes études.",
        },
        project_diva: {
            summary: "Refactoring frontend et backend du site officiel de l'association étudiante de l'ISFATES: la DIVA.",
        },
        project_walkinlove: {
            summary: "Réalisation d'un site web avec php dans le cadre de mes études.",
        },
        project_zukunftsforum: {
            summary: "Mise en place d'un site web WordPress ainsi que d'une charte graphique pour l'association 'Forum d'avenir'.",
        }
    },
    en: {
        title: "My projects",
        project_caretaker: {
            summary: "This is my 3rd year of study project at ISFATES. We had to create a smart home model connected to a progressive web application. I was the project manager and the IT manager of the project.",
        },
        project_adventskalender: {
            summary: "IT manager for the creation of a progressive web app simulating an advent calendar for ISFATES students. The money collected will be donated to several associations.",
        },
        project_astropulse: {
            summary: "Creation of an online survey showing that the frequency of a pulsar can influence our perception of time. Project carried out as part of my studies.",
        },
        project_diva: {
            summary: "Refactoring frontend and backend of the official website of the ISFATES student association: the DIVA.",
        },
        project_walkinlove: {
            summary: "Realization of a website with php as part of my studies.",
        },
        project_zukunftsforum: {
            summary: "Setting up a WordPress website and a graphic charter for the association 'Forum d'avenir'.",
        }
    }
}

export default LOCALIZE;