const LOCALIZE = {
    de: {
        check_repo: "Sehen Sie den Quellcode",
        technologies_used: "Verwendete Technologien",
    },
    fr: {
        check_repo: "Voir le code source",
        technologies_used: "Technologies utilisées",
    },
    en: {
        check_repo: "View source code",
        technologies_used: "Technologies used",
    }
}

export default LOCALIZE;