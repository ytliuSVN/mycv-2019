const LOCALIZE = {
    de: {
        title: "Kontakt",
        send: "Senden",
        email_has_been_sent: "Ihre E-Mail wurde erfolgreich an mich gesendet! Ich würde mich freuen, sie zu beantworten.",
        subject: "Betreff...",
        from: "Ihre E-Mailadresse...",
        message: "Nachricht...",
        download_cv: "- Papierversion herunterladen -",
        invalid_email_form: "Bitte geben Sie eine gültige E-Mail, einen Betreff und eine Nachricht ein!",
    },
    fr: {
        title: "Contact",
        send: "Envoyer",
        email_has_been_sent: "Your email has been successfully sent to me! I would be happy to answer them.",
        subject: "Sujet...",
        from: "Votre email...",
        message: "Message...",
        download_cv: "- télécharger la version papier -",
        invalid_email_form: "Veuillez entrer un email valides, un sujet et un message !",
    },
    en: {
        title: "Contact",
        send: "Send",
        email_has_been_sent: "Your email has been successfully sent to me! I would be happy to answer them.",
        subject: "Subject...",
        from: "From...",
        message: "Message...",
        download_cv: "- download the paper version -",
        invalid_email_form: "Please enter a valid email, subject and message !",
    },
};

export default LOCALIZE;