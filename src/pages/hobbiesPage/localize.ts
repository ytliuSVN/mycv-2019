const LOCALIZE = {
    de: {
        title: "Meine Hobbys",
        play_music: "Musik spielen (Gitarre, Klavier)",
        dive: "Tauchen (CMAS1 Level)",
        programming: "Webprogrammierung (Typescript, Python, PHP)",
        react_lover: "React lover",
        make_icon: "Erstellung von Icons",
        snowboard: "Snowboarden",
    },
    fr: {
        title: "Mes loisirs",
        play_music: "Jouer de la musique (guitare, piano)",
        dive: "Plongée (niveau CMAS1)",
        programming: "Programmation web (Typescript, Python, PHP)",
        react_lover: "React fan",
        make_icon: "Création d'icônes",
        snowboard: "Snowboard ",
    },
    en: {
        title: "My hobbies",
        play_music: "Play music (guitar, piano)",
        dive: "Diving (CMAS1 level)",
        programming: "Web programming (Typescript, Python, PHP)",
        react_lover: "React lover",
        make_icon: "Icons creation",
        snowboard: "Faire du snowboard",
    }
}

export default LOCALIZE;