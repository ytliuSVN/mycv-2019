const LOCALIZE = {
    de: {
        title: "Fähigkeiten",
        languages: "Sprachen",
        french: "Französisch",
        german: "Deutsch",
        english: "Englisch",
    },
    fr: {
        title: "Compétences",
        languages: "Langues",
        french: "Français",
        german: "Allemand",
        english: "Anglais",
    },
    en: {
        title: "Skills",
        languages: "Languages",
        french: "French",
        german: "German",
        english: "English",
    }
}

export default LOCALIZE;