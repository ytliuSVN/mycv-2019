const LOCALIZE = {
    de: {
        introduction_text: "\"Als 20-jähriger Student am Deutsch-Französischen Hochschulinstitut für Technik und Wirtschaft (DFHI) interessiere ich mich nun seit einigen Jahren für Webprogrammierung und Internet. Ehrgeizig und zuverlässig, hoffe ich, dem Profil zu entsprechen, das Sie suchen.\"",
        swipe_indication: "Swipen Sie nach rechts, um meinen Lebenslauf einzusehen",
        student: "Student der Informatik",
    },
    fr: {
        introduction_text: "\"Étudiant de 20 ans à l'Institut Supérieur Franco-Allemand de Techniques, d’Économies et de Sciences(ISFATES), je me passionne depuis plusieurs annéees déjà pour la programmation et le web plus précisement. Ambitieux et volontaire, j'espère correspondre au profil que vous recherchez.\"",
        swipe_indication: "Balayez vers la droite pour parcourir mon CV",
        student: "Étudiant en Informatique",
    },
    en: {
        introduction_text: "\"As a 20-year - old student at the Institut Supérieur Franco-Allemand de Techniques, d'Économies et de Sciences (ISFATES), I have been passionate about programming and the web for several years now. Ambitious and willing, I hope to fit the profile you're looking for.\"",
        swipe_indication: "Swipe to the right to browse my CV",
        student: "Computer Science Student",
    }
}

export default LOCALIZE;