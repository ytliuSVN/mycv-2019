const LOCALIZE = {
    de: {
        presentation: "Vorstellung",
        background: "Ausbildung",
        skills: "Fähigkeiten",
        personal_experiences: "Erfahrungen",
        projects: "Projekte",
        hobbies: "Hobbys",
        contacts: "Kontakt",
        check_repo: "Sehen Sie den Quellcode",
    },
    fr: {
        presentation: "Présentation",
        background: "Formation",
        skills: "Compétences",
        personal_experiences: "Expériences personnelles",
        projects: "Projets",
        hobbies: "Loisirs",
        contacts: "Contact",
        check_repo: "Voir le code source",
    },

    en: {
        presentation: "Presentation",
        background: "Background",
        skills: "Skills",
        personal_experiences: "Personal experiences",
        projects: "Projects",
        hobbies: "Hobbies",
        contacts: "Contact",
        check_repo: "View source code",
    },
};

export default LOCALIZE;