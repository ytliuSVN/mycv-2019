const COMMON_LOCALIZE = {
    de: {
        email_has_been_sent: "Ihre E-Mail wurde erfolgreich gesendet !",
        send: "Senden",
        french: "Französisch",
        german: "Deutsch",
        english: "Englisch",
        feature_coming_soon: "Feature folgt in Kürze !",
    },
    fr: {
        email_has_been_sent: "Votre email a été envoyé avec succès !",
        send: "Envoyer",
        french: "Français",
        german: "Allemand",
        english: "Anglais",
        feature_coming_soon: "Fonctionnalité à venir !",
    },
    en: {
        email_has_been_sent: "Your email has been successfully sent !",
        send: "Send",
        french: "French",
        german: "German",
        english: "English",
        feature_coming_soon: "Feature coming soon !",
    },
};

export default COMMON_LOCALIZE;